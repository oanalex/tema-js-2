Vue.component('datatable', {
  props: ['result', 'selectedrow'],
  methods: {
    savei: function() {
      this.$emit("getindex", this.selectedrow);
      this.selectedrow = "";
    }
  },    
  template: 
  `
  <div id="data-table" class="grid-container">
    <div class="grid-item">
        {{result.id}}
    </div>
    <div  class="grid-item">
        {{result.nume}}
    </div>
    <div class="grid-item">
        {{result.adresa}}
    </div>
    <div class="grid-item">
        {{result.email}}
    </div>
    <div class="grid-item">
        {{result.varsta}}
    </div>        
    <div class="grid-item">
        <button type="button" class="btn btn-primary"data-toggle=modal data-target="#edit-row"  @click="savei">
            Editeaza
        </button>
        <button type="button" class="btn btn-danger" data-toggle=modal data-target="#delete-row" @click="savei">
            Sterge
        </button>
    </div>
  </div>
  `
})

Vue.component('edit-row', {
  props: ['rowindex', 'result', 'errors'],
    data: function() {
    return {
      id: {
        type: Number
      },
      nume: {
        type: String
      },
      adresa: {
        type: String
      },
      email: {
        type: String
      },
      varsta: {
        type: Number
      }
    };
  },
  watch: {
    result: function() {
      this.id = (this.result.id);
      this.nume = (this.result.nume);
      this.adresa = (this.result.adresa);
      this.email = (this.result.email);
      this.varsta = (this.result.varsta);
    }
  },    
  methods:{
    checkForm:function(e) {
      e.preventDefault();
      this.errors = [];
      if (!this.nume) {
        console.log(this.nume.val);
        this.errors.push("Completati numele.");
      }
      if (!this.adresa) {
        this.errors.push("Completati adresa.");
      }      
      if (!this.email) {
        this.errors.push('Email required.');
      } else if (!this.validEmail(this.email)) {
        this.errors.push('Valid email required.');
      }
      if (!this.varsta) {
        this.errors.push("Completati varsta.");
      }            
      if (!this.errors.length) {

        this.result.id = (this.id);
        this.result.nume = (this.nume);
        this.result.adresa = (this.adresa);
        this.result.email = (this.email);
        this.result.varsta = (this.varsta);
        const elem = this.$refs.myBtn
        elem.click()       
      }


    },
    validEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
  },
  template: 
  `
  <div id="edit-row" class="modal fade">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Editati randul</h4>
          <button type="button" class="close" data-dismiss="modal" ref="myBtn">&times;</button>
        </div>
        <div class="modal-body">
          <form @submit="checkForm" action="" novalidate="true">
            <p v-if="errors.length">
              <b>Va rugam sa corectati erorile:</b>
              <ul>
                <li v-for="error in errors">{{ error }}</li>
              </ul>
            </p>
            <div class="form-group">
              <label for="id">Id:</label>
              <input type="number" class="form-control" name="id" disabled="true" v-model="id">
            </div>
            <div class="form-group">
              <label for="nume">Nume:</label>
              <input type="text" class="form-control" name="nume" v-model="nume">
            </div>
            <div class="form-group">
              <label for="adresa">Adresa:</label>
              <input type="text" class="form-control" name="adresa" v-model="adresa">
            </div>    
            <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" name="email" v-model="email">
            </div>
            <div class="form-group">
              <label for="varsta">Varsta:</label>
              <input type="number" class="form-control" name="varsta" v-model="varsta">
            </div>
            <button type="submit" class="btn btn-primary"">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  `
})

Vue.component('delete-row', {
  props: ['rowindex'],  
  template: 
  `
  <div class="modal fade" id="delete-row">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">     
        <div class="modal-header">
          <h4 class="modal-title">Sunteti sigur ca doriti sa stergeti intrarea?</h4>
        </div>      
        <div class="modal-footer">
          <button @click="$emit(\'remove\')" type="button" class="btn btn-danger" data-dismiss="modal">Da</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Nu</button>
        </div>
        
      </div>
    </div>
  </div>

  `
})


new Vue({
  el: '#app',
  data: {
    results: [],
    errors: [],
    childdata: "",
    function() {
      return{
        childdata: ""
      };
    }
  },
  created: function () {
    // Alias the component instance as `vm`, so that we  
    // can access it inside the promise function
    var vm = this
    // Fetch our array of posts from an API
    fetch('http://localhost:3000/clienti')
      .then(function (response) {
        return response.json()
      })
      .then(function (data) {
        vm.results = data
      })
  },
  methods: {
    updateIndex(variable) {
      this.childdata = variable;
    }
  }
})

